﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameramovement : MonoBehaviour
{
    public float step = 0.1f;
    float currentY;
    int counter = 0;
    public float timeToWait = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        currentY = transform.position.y;
        StartCoroutine(WaitAndMove(timeToWait));
    }

    IEnumerator WaitAndMove(float timeToWait)
    {   while (true)
        {
            float currentStep;
            /*fake comment*/

            if (counter >= 0 && counter < 4)
            {
                currentStep = step;
            }
            else
            {
                currentStep = -step;
            }

            
            if (counter >= 7)
            {
                counter = 0;
            }
            else
            {
                counter += 1;
            }

            currentY += currentStep;
            transform.position = new Vector3(transform.position.x, currentY, transform.position.z);


            yield return new WaitForSeconds(timeToWait);
        }
  
    }

}
