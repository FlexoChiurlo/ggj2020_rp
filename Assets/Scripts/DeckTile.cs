﻿using UnityEngine;

class DeckTile : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer breachRenderer;

    [SerializeField]
    private Sprite TileIntact;

    [SerializeField]
    private Sprite TileBroken;

    [SerializeField]
    private GameObject Fountain;

    [Header("Audio")]
    public AudioClip[] ship_break_sfx;

    [SerializeField]
    private AudioSource aux;

    private bool isDamaged;

    public bool IsDamaged
    {
        get
        {
            return isDamaged;
        }
    }

    public void Damage()
    {
        //play break animation or vfx
        int whichClip = Random.Range(0, ship_break_sfx.Length);
        aux.clip = ship_break_sfx[whichClip];
        aux.Play();

        isDamaged = true;
        breachRenderer.sprite = TileBroken;
    }

    public void Repair()
    {
        isDamaged = false;
        breachRenderer.sprite = TileIntact;
    }

    public void SetFountain(bool status)
    {
        Fountain.SetActive(status);
    }
}
