﻿using IndieMarc.Platformer;
using UnityEngine;

public class Ladder : MonoBehaviour
{
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            other.transform.parent.GetComponent<PlayerCharacter>().isNearLadder = true;
            other.transform.parent.GetComponent<CapsuleCollider2D>().enabled = false;

        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            other.transform.parent.GetComponent<PlayerCharacter>().isNearLadder = false;
            other.transform.parent.GetComponent<CapsuleCollider2D>().enabled = true;

        }
    }
}
