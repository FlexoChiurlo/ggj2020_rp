﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    public Vector2 speed = new Vector2(0,10f);
    public Vector2 accel = new Vector2(0, 5f);
    private Rigidbody2D rigid;
    public float ballTimeToDestroy = 1f;

    [Header("Audio")]
    public AudioClip fischio_sfx;

    [SerializeField]
    private AudioSource aux;

    [SerializeField]
    private GameObject BoomPrefab;

    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        int numrand = Random.Range(1, 2);
        if (numrand == 1)
        {
            aux.clip = fischio_sfx;
            aux.Play();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 move = rigid.velocity;
        move.x = Mathf.MoveTowards(move.x, speed.x, accel.x * Time.fixedDeltaTime);
        move.y = Mathf.MoveTowards(move.y, speed.y, accel.y * Time.fixedDeltaTime);
        rigid.velocity = move;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        DeckTile deckTile;
        if (collision.TryGetComponent(out deckTile))
        {
            if (!deckTile.IsDamaged)
            {
                deckTile.Damage();

                //TODO animazione palla che esplode


                StartCoroutine(RemoveBall());
            }
        }

        if (collision.CompareTag("Player") && collision.gameObject.GetComponent<PirateInteraction>().pirateState != PirateState.Stunned)
        {
            collision.gameObject.GetComponent<PirateInteraction>().Stun();
            StartCoroutine(RemoveBall());
        }
    }

    IEnumerator RemoveBall()
    {
        yield return new WaitForSeconds(ballTimeToDestroy);
        Instantiate(BoomPrefab, transform.position, Quaternion.identity);
        GameObject.Destroy(this.gameObject);
    }

}
