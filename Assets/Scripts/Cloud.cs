﻿using UnityEngine;

public class Cloud : MonoBehaviour
{
    public Transform start;
    public Transform end;
    public Transform cloud;

    public float speed;

    private float startX;
    private float endX;
    // Start is called before the first frame update
    void Start()
    {
        startX = start.localPosition.x;
        endX = end.localPosition.x;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = cloud.localPosition;
        pos.x += speed * Time.deltaTime;
        if (pos.x > endX)
        {
            pos.x = startX;
        }
        cloud.localPosition = pos;
    }
}
