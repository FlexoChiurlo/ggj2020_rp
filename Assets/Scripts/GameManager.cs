﻿using IndieMarc.Platformer;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[Serializable]
struct InitialText
{
    public string text;
    public float waitInSeconds;
}
class GameManager : MonoBehaviour
{
    public float gameDurationInMinutes = 0f;
    public Text readySetGotext;
    public Text countdownText;
    public GameObject victoryImage;
    public Image lossImage;
    public InitialText[] initialTexts;
    public CannonBallSpawner krakenSpawner;
    public CannonBallSpawner spawner;

    public GameObject[] players;

    public DeckTile[] deck1;
    public DeckTile[] deck2;
    public DeckTile[] deck3;
    public DeckTile[] deck4;

    //counting from the bottom up
    private bool[] isLevelTotallyFlooded = new bool[4];

    public float maximumFloodingSpeed = 1f;

    public float Health = 300f;

    public Transform sea;
    public Transform seaStartPos;
    public Transform seaEndPos;

    public Transform flood;
    public Transform floodStartPos;
    public Transform floodEndPos;

    private float countdown;
    private float seaStartY;
    private float seaEndY;
    private float floodStartY;
    private float floodEndY;

    [SerializeField]
    AudioSource aux;

    [SerializeField]
    Animator EndingAnim;

    enum GamePhase
    {
        ReadySetGo,
        Playing,
        End
    }
    private GamePhase gamePhase = GamePhase.ReadySetGo;
    private uint currentInitialTextIndex = 0;
    private float currentInitialTextWait = 0f;

    void Start()
    {
        countdown = gameDurationInMinutes * 60f;
        victoryImage.SetActive(false);
        lossImage.enabled = false;
        readySetGotext.text = initialTexts[currentInitialTextIndex].text;
        countdownText.text = countdown.ToString();
        seaStartY = seaStartPos.position.y;
        seaEndY = seaEndPos.position.y;
        floodStartY = floodStartPos.position.y;
        floodEndY = floodEndPos.position.y;

        foreach (GameObject playerObject in players)
        {
            playerObject.GetComponent<PlayerCharacter>().DisableControls();
        }
    }

    void Update()
    {
        switch (gamePhase)
        {
            case GamePhase.ReadySetGo:
                currentInitialTextWait += Time.deltaTime;
                if (currentInitialTextWait >= initialTexts[currentInitialTextIndex].waitInSeconds)
                {
                    currentInitialTextWait = 0f;

                    ++currentInitialTextIndex;
                    if (currentInitialTextIndex >= initialTexts.Length)
                    {
                        gamePhase = GamePhase.Playing;
                        aux.Play();
                        foreach (GameObject playerObject in players)
                        {
                            playerObject.GetComponent<PlayerCharacter>().EnableControls();
                        }

                        spawner.Locked = false;
                        krakenSpawner.Locked = false;
                        readySetGotext.enabled = false;
                    }
                    else
                    {
                        readySetGotext.text = initialTexts[currentInitialTextIndex].text;
                    }
                }
                break;
            case GamePhase.Playing:
                countdown -= Time.deltaTime;

                if (countdown <= 0f)
                {
                    Victory();
                }
                else
                {
                    countdownText.text = countdown.ToString("F1");
                }
                break;
            case GamePhase.End:
                if (Input.anyKeyDown)
                {
                    SceneManager.LoadScene("MainMenu");
                }
                break;
        }
    }

    DeckTile[] GetLowestNotCompletelyDamagedDeck()
    {
        if (!isLevelTotallyFlooded[3])
        {
            return deck4;
        }

        if (!isLevelTotallyFlooded[2])
        {
            return deck3;
        }

        if (!isLevelTotallyFlooded[1])
        {
            return deck2;
        }

        if (!isLevelTotallyFlooded[0])
        {
            return deck1;
        }

        return null;
    }

    private void FixedUpdate()
    {
        if (gamePhase == GamePhase.Playing)
        {
            if (ArePiratesAllStunned() || Health <= 0f)
            {
                GameOver();
            }

            if (Health < 200 && !isLevelTotallyFlooded[3])
            {
                isLevelTotallyFlooded[3] = true;

                //Lock Camera
            }
            if (Health < 100 && !isLevelTotallyFlooded[2])
            {
                isLevelTotallyFlooded[2] = true;

                //Lock Camera

            }
            /*test*/
            Health -= maximumFloodingSpeed * CheckDamage(GetLowestNotCompletelyDamagedDeck()) * Time.fixedDeltaTime;
            Health = Mathf.Clamp(Health, 0f, 300f);
            sea.position = new Vector2(sea.position.x, Mathf.Lerp(seaStartY, seaEndY, 1f - Health / 300f));
            flood.position = new Vector2(flood.position.x, Mathf.Lerp(floodStartY, floodEndY, 1f - Health / 300f));
        }
    }

    float CheckDamage(DeckTile[] deckTiles)
    {
        float damagedTiles = 0f;

        foreach (DeckTile dT in deckTiles)
        {
            if (dT.IsDamaged)
            {
                ++damagedTiles;
                dT.SetFountain(true);
            }
            else
            {
                dT.SetFountain(false);
            }
        }

        return damagedTiles / deckTiles.Length;
    }

    private bool ArePiratesAllStunned()
    {
        foreach (GameObject p in players)
        {
            if (p.GetComponentInChildren<PirateInteraction>().pirateState != PirateState.Stunned)
            {
                return false;
            }
        }

        return true;
    }

    private void GameOver()
    {
        countdown = 0f;
        countdownText.enabled = false;
        lossImage.enabled = true;

        foreach (GameObject playerObject in players)
        {
            playerObject.GetComponent<PlayerCharacter>().DisableControls();
        }

        spawner.Locked = true;
        krakenSpawner.Locked = true;
        gamePhase = GamePhase.End;

    }

    void Victory()
    {
        countdown = 0f;
        countdownText.enabled = false;
        victoryImage.SetActive(true);
        EndingAnim.SetTrigger("Ending");

        foreach (GameObject playerObject in players)
        {
            playerObject.GetComponent<PlayerCharacter>().DisableControls();
        }

        spawner.Locked = true;
        krakenSpawner.Locked = true;
        gamePhase = GamePhase.End;
    }

}
