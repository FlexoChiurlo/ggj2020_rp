﻿using UnityEngine;

static class Utils
{
    public static bool IsCloserToValThan(float fromVal, float testedVal, float thanVal)
    {
        return Mathf.Abs(fromVal - testedVal) <= Mathf.Abs(fromVal - thanVal);
    }
}
