﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tentacle : MonoBehaviour
{
    public Vector2 speed = new Vector2(0, 10f);
    public Vector2 accel = new Vector2(0, 5f);
    private Rigidbody2D rigid;
    public float tentacleTimeToDestroy = 1f;

    public bool isRetiring = false;

    public Vector2 speedRetract = new Vector2(0, 20f);
    public Vector2 accelRetract = new Vector2(0, 1000000f);

    private Transform maxTentacleHeight;

    [Header("Audio")]
    public AudioClip ship_break_sfx;
    public AudioClip fischio_sfx;

    [SerializeField]
    private AudioSource aux;

    [SerializeField]
    private GameObject BoomPrefab;



    // Start is called before the first frame update
    void Start()
    {
        maxTentacleHeight = GameObject.FindGameObjectWithTag("MaxTentacleHeight").transform;
        rigid = GetComponent<Rigidbody2D>();

         aux.Play();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(GetComponent<Collider2D>().bounds.max.y > maxTentacleHeight.position.y)
        {
            StartCoroutine(RemoveBall());
        }
        if (!isRetiring)
        {
            Vector2 move = rigid.velocity;
            move.x = Mathf.MoveTowards(move.x, speed.x, accel.x * Time.fixedDeltaTime);
            move.y = Mathf.MoveTowards(move.y, speed.y, accel.y * Time.fixedDeltaTime);
            rigid.velocity = move;
        }
        else
        {
            Vector2 move = rigid.velocity;
            move.x = Mathf.MoveTowards(move.x, speedRetract.x, accelRetract.x * Time.fixedDeltaTime);
            move.y = Mathf.MoveTowards(move.y, speedRetract.y, accelRetract.y * Time.fixedDeltaTime);
            rigid.velocity = move;

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        DeckTile deckTile;
        if (collision.TryGetComponent(out deckTile))
        {
            if (!deckTile.IsDamaged)
            {
                deckTile.Damage();

                //TODO animazione palla che esplode


                StartCoroutine(RemoveBall());
            }
        }

        if (collision.CompareTag("Player") && collision.gameObject.GetComponent<PirateInteraction>().pirateState != PirateState.Stunned)
        {
            collision.gameObject.GetComponent<PirateInteraction>().Stun();
            StartCoroutine(RemoveBall());
        }
    }

    IEnumerator RemoveBall()
    {
        isRetiring = true;
        rigid.velocity = Vector2.zero;
        yield return new WaitForSeconds(tentacleTimeToDestroy);
        Instantiate(BoomPrefab, transform.position, Quaternion.identity);
        GameObject.Destroy(this.gameObject);
    }

}
