﻿using System.Collections;
using UnityEngine;

public class CannonBallSpawner : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject cannonBall;
    public float[] specificProb;
    public float maxTimeLapse = 1f;
    public float minTimeLapse = 0.5f;
    public bool needToSpawn = true;
    // Start is called before the first frame update
    public bool Locked { get; set; }
    void Start()
    {
        spawnPoints = GetComponentsInChildren<Transform>();
        Locked = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Locked == false)
        {

            if (needToSpawn)
            {
                float nextBallTime = Random.Range(minTimeLapse, maxTimeLapse);
                float nextSpawnPointProb = Random.value;
                int nextSpawn = -1;
                for (int i = 0; i < specificProb.Length; i++)
                {

                    if (specificProb[i] >= nextSpawnPointProb)
                    {
                        nextSpawn = i;
                        break;
                    }
                }
                needToSpawn = false;
                StartCoroutine(SpawnBall(nextBallTime, nextSpawn));
            }
        }
    }
    IEnumerator SpawnBall(float nextBallTime, int nextSpawn)
    {
        yield return new WaitForSeconds(nextBallTime);
        GameObject.Instantiate(cannonBall, spawnPoints[nextSpawn].position, Quaternion.identity);
        needToSpawn = true;
    }


}
