﻿using UnityEngine;

struct RepairAction
{
    private float repairDuration;

    private float timeSpentRepairing;
    private DeckTile reparationTarget;

    public RepairAction(float _repairDuration)
    {
        timeSpentRepairing = 0;
        repairDuration = _repairDuration;
        reparationTarget = null;
    }

    public PirateState UpdateAction(Collider2D other, float currentX, float timeStep)
    {
        DeckTile deckTile = other.GetComponent<DeckTile>();

        if (deckTile != null && deckTile.IsDamaged)
        {
            if (deckTile == reparationTarget)
            {
                Debug.Log("repair old");

                timeSpentRepairing += timeStep;

                if (timeSpentRepairing >= repairDuration)
                {
                    Debug.Log("finish");
                    reparationTarget.Repair();
                    Stop();
                }
            }
            else if (reparationTarget == null
                || Utils.IsCloserToValThan(currentX, deckTile.transform.position.x, reparationTarget.transform.position.x))
            {
                Debug.Log("repair new");
                timeSpentRepairing = 0f;
                reparationTarget = deckTile;
            }

            return PirateState.Repairing;
        }


        return PirateState.Normal;
    }

    public void Stop(Collider2D other)
    {
        if (reparationTarget.gameObject == other.gameObject)
        {
            Debug.Log("conditional stop");
            Stop();
        }
    }

    public void Stop()
    {
        Debug.Log("stop");
        timeSpentRepairing = 0f;
        reparationTarget = null;
    }
}