﻿using UnityEngine;
using UnityEngine.SceneManagement;

class MainMenuManager : MonoBehaviour
{
    [SerializeField]
    private GameObject tutorialScreen;

    // Start is called before the first frame update
    void Start()
    {
        tutorialScreen.SetActive(false);
    }

    void Update()
    {
        if (Input.anyKeyDown)
        {
            if (tutorialScreen.activeInHierarchy)
            {
                SceneManager.LoadScene(1);
            }
            else
            {
                tutorialScreen.SetActive(true);
            }
        }
    }
}
