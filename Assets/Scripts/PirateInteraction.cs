﻿using IndieMarc.Platformer;
using UnityEngine;


public enum PirateState
{
    HasRum,
    Stunned,
    Normal,
    Repairing,
}

public class PirateInteraction : MonoBehaviour
{
    [Header("Meta Configuration")]

    public string rumTag = "RUM";
    public string pirateTag = "PIRATE";

    [Header("Configuration")]

    public PirateState pirateState = PirateState.Normal;
    public float timeStunned = 10f;
    public float repairDuration = 2f;

    [SerializeField]
    private CharacterAnim characterAnim;

    private int playerID;
    PlayerControls controls;

    private PlayerCharacter pc;

    private RepairAction repairAction;

    [Header("Audio")]
    public AudioClip[] stun_sfx;
    public AudioClip[] repair_sfx;
    public AudioClip drink_rum_sfx;
    public AudioClip grab_rum_sfx;
    [SerializeField]
    private AudioSource aux;

    #region Monobehaviour

    // Start is called before the first frame update
    void Start()
    {
        repairAction = new RepairAction(repairDuration);
        pc = transform.parent.GetComponent<PlayerCharacter>();
        playerID = pc.player_id;
        controls = PlayerControls.Get(playerID);
        InvokeRepeating("PlaySoundRepair", 0.0f, 0.25f);
    }

    void FixedUpdate()
    {
        if (pirateState == PirateState.Repairing)
        {
            if (controls.GetActionHold() == false)
            {
                pirateState = PirateState.Normal;
                repairAction.Stop();

            }
        }
    }

    #endregion

    #region Public Method

    public void Stun()
    {
        pirateState = PirateState.Stunned;
        pc.DisableControls();
        repairAction.Stop();
        DropRum();
        characterAnim.Knockout();
        int whichClip = Random.Range(0, stun_sfx.Length);
        aux.clip = stun_sfx[whichClip];
        aux.Play();
        Invoke("FinishStunned", timeStunned);
    }

    public void Heal()
    {
        FinishStunned();
    }

    #endregion

    #region Private Methods

    private void FinishStunned()
    {
        CancelInvoke("FinishStunned");
        pirateState = PirateState.Normal;
        pc.EnableControls();
        characterAnim.Heal();

        aux.clip = drink_rum_sfx;
        aux.Play();
    }

    private void GrabRum()
    {
        aux.clip = grab_rum_sfx;
        aux.Play();
    }

    private void DropRum()
    {

    }

    #endregion

    #region Interaction
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == pirateTag && pirateState == PirateState.HasRum)
        {
            PirateInteraction otherPirate = other.GetComponent<PirateInteraction>();
            if (otherPirate?.pirateState == PirateState.Stunned)
            {
                otherPirate.Heal();
                pirateState = PirateState.Normal;
            }
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (pirateState != PirateState.Stunned)
        {
            if (controls.GetActionDown())
            {
                if (other.tag == rumTag && pirateState == PirateState.Normal)
                {
                    pirateState = PirateState.HasRum;
                    GrabRum();
                }
                /*else if (other.tag == pirateTag && pirateState == PirateState.HasRum)
                {
                    PirateInteraction otherPirate = other.GetComponent<PirateInteraction>();
                    if (otherPirate?.pirateState == PirateState.Stunned)
                    {
                        otherPirate.Heal();
                    }
                }*/
            }
            else if (controls.GetActionHold() && pirateState != PirateState.HasRum)
            {
                pirateState = repairAction.UpdateAction(other, transform.position.x, Time.fixedDeltaTime);

            }
            else if (controls.GetCancelDown())
            {
                pirateState = PirateState.Normal;
                DropRum();
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (pirateState == PirateState.Repairing)
        {
            pirateState = PirateState.Normal;
            repairAction.Stop(other);
        }
    }

    void PlaySoundRepair()
    {
        if (pirateState == PirateState.Repairing)
        {
            int whichClip = Random.Range(0, repair_sfx.Length);
            aux.clip = repair_sfx[whichClip];
            aux.Play();
        }
    }
    #endregion
}
