﻿using UnityEngine;
using UnityEngine.UI;

class TextSinPulse : MonoBehaviour
{
    public float angularSpeed;
    public Text pulsingText;

    private float currentAngle = 0f;

    void Update()
    {
        currentAngle += Time.deltaTime * angularSpeed;
        currentAngle = currentAngle >= 2 * Mathf.PI ? currentAngle - 2 * Mathf.PI : currentAngle;

        Color currentColor = pulsingText.color;
        currentColor.a = 1 + Mathf.Sin(currentAngle) / 2;
        pulsingText.color = currentColor;
    }
}
