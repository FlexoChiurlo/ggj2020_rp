﻿using UnityEngine;

class ScrollMovement : MonoBehaviour
{
    public SpriteRenderer[] seaTiles;
    public float speed;
    public float angularSpeed;
    public float amplitude;
    public Transform startPoint;
    public Transform endPoint;

    private float startX;
    private float endX;
    private float centerY;
    private float padding;
    private float currentAngle;
    void Start()
    {
        startX = startPoint.localPosition.x;
        endX = endPoint.localPosition.x;
        padding = seaTiles[0].bounds.size.x;
        centerY = seaTiles[0].transform.localPosition.y;
        currentAngle = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 step = Vector3.right * Time.deltaTime * speed;
        currentAngle += Time.deltaTime * angularSpeed;
        currentAngle = currentAngle >= 2 * Mathf.PI ? currentAngle - 2 * Mathf.PI : currentAngle;
        float currentY = centerY + Mathf.Sin(currentAngle) * amplitude;

        foreach (SpriteRenderer seaTile in seaTiles)
        {
            Vector3 pos = seaTile.transform.localPosition;
            pos.y = currentY;
            pos += step;
            seaTile.transform.localPosition = pos;
        }

        for (int i = seaTiles.Length - 1; i >= 0; --i)
        {
            SpriteRenderer seaTile = seaTiles[i];
            if (seaTile.transform.localPosition.x >= endX)
            {
                int next = i + 1;
                next = next == seaTiles.Length ? 0 : next;

                Vector3 pos = seaTile.transform.localPosition;

                pos.x = seaTiles[next].transform.localPosition.x - padding;

                seaTile.transform.localPosition = pos;
            }
        }
    }
}