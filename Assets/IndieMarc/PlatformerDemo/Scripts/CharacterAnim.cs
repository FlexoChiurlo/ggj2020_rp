﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IndieMarc.Platformer
{
    [RequireComponent(typeof(PlayerCharacter))]
    [RequireComponent(typeof(Animator))]
    public class CharacterAnim : MonoBehaviour
    {
        private PlayerCharacter character;
        private CharacterHoldItem character_item;
        private Animator animator;

        private PirateInteraction pirateInteraction;

        void Awake()
        {
            character = GetComponent<PlayerCharacter>();
            character_item = GetComponent<CharacterHoldItem>();
            animator = GetComponent<Animator>();
            pirateInteraction = GetComponentInChildren<PirateInteraction>();

            character.onJump += OnJump;
            character.onCrouch += OnCrouch;
        }

        void FixedUpdate()
        {

            //Anims
            animator.SetFloat("Speed", character.GetMove().magnitude);
            animator.SetBool("Hold", pirateInteraction.pirateState == PirateState.HasRum);
            animator.SetBool("Ladder", character.isClimbing);
            animator.SetBool("Hammer", pirateInteraction.pirateState == PirateState.Repairing);


        }

        public void Knockout()
        {
            animator.SetTrigger("Knock");
        }

        public void Heal()
        {
            animator.SetTrigger("Raise");
        }

        void OnCrouch()
        {
            animator.SetTrigger("Crouch");
        }

        void OnJump()
        {
            animator.SetTrigger("Jump");
        }
    }

}