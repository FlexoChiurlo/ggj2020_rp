﻿using System.Collections.Generic;
using UnityEngine;
using Rewired;

/// <summary>
/// Player controls for platformer demo
/// Author: Indie Marc (Marc-Antoine Desbiens)
/// </summary>

namespace IndieMarc.Platformer
{

    public class PlayerControls : MonoBehaviour
    {
        public int player_id;
        private Player player; // The Rewired Player

        public KeyCode left_key;
        public KeyCode right_key;
        public KeyCode up_key;
        public KeyCode down_key;
        public KeyCode jump_key;
        public KeyCode action_key;


        public KeyCode cancel_key;

        private Vector2 move = Vector2.zero;
        private bool jump_press = false;
        private bool jump_hold = false;
        private bool action_press = false;
        private bool action_hold = false;
        private bool cancel_press = false;

        private static Dictionary<int, PlayerControls> controls = new Dictionary<int, PlayerControls>();

        void Awake()
        {
            controls[player_id] = this;
            player = ReInput.players.GetPlayer(player_id);
        }

        void OnDestroy()
        {
            controls.Remove(player_id);
        }

        void FixedUpdate()
        {

            move = Vector2.zero;
            jump_hold = false;
            jump_press = false;
            action_hold = false;
            action_press = false;
            cancel_press = false;



            if (player.GetAxis("MoveHorizontal") > 0.1f)
            {
                move += Vector2.right;
            }

            if (player.GetAxis("MoveHorizontal") < -0.1f)
            {
                move += -Vector2.right;
            }
            
            if (player.GetAxis("MoveVertical") > 0.1f)
            {
                move += Vector2.up;
            }
            if (player.GetAxis("MoveVertical") < -0.1f)
            {
                move += -Vector2.up;
            }



            /*if (Input.GetKey(jump_key))
            {
                jump_hold = true;
            }
            if (Input.GetKeyDown(jump_key))
            {
                jump_press = true;
            }*/
            /*
            if (Input.GetKey(action_key))
            {
                action_hold = true;
            }
            if (Input.GetKeyDown(action_key))
            {
                action_press = true;
            }

            if (Input.GetKeyDown(cancel_key))
            {
                cancel_press = true;
            }
            */
            if (player.GetButton("Action"))
            {
                action_hold = true;
                /*try*/
            }


            if (player.GetButtonDown("Action"))
            {
                action_press = true;
            }

            if (player.GetButtonDown("Cancel"))
            {
                cancel_press = true;
            }


            float move_length = Mathf.Min(move.magnitude, 1f);
            move = move.normalized * move_length;
        }



        public Vector2 GetMove()
        {
            return move;
        }
        public bool GetActionDown()
        {
            return action_press;
        }

        public bool GetActionHold()
        {
            return action_hold;
        }

        public bool GetCancelDown()
        {
            return cancel_press;
        }


        public static PlayerControls Get(int player_id)
        {
            foreach (PlayerControls control in GetAll())
            {
                if (control.player_id == player_id)
                {
                    return control;
                }
            }
            return null;
        }

        public static PlayerControls[] GetAll()
        {
            PlayerControls[] list = new PlayerControls[controls.Count];
            controls.Values.CopyTo(list, 0);
            return list;
        }

    }

}